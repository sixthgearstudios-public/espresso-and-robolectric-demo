package com.example.calculator;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends Activity {

    private EditText txtNumber1, txtNumber2;
    private TextView result;
    private Button btnAdd, btnSubtract, btnMultiply, btnDivide;
    public static final String DLG_MESSAGE = "Input wrong value";
    public static final String DLG_TITLE = "Alert";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        txtNumber1 = (EditText) findViewById(R.id.txtNumber1);
        txtNumber2 = (EditText) findViewById(R.id.txtNumber2);
        result = (TextView) findViewById(R.id.txtResult);
        btnAdd = (Button) findViewById(R.id.btnAdd);
        btnSubtract = (Button) findViewById(R.id.btnSubtract);
        btnMultiply = (Button) findViewById(R.id.btnMultiply);
        btnDivide = (Button) findViewById(R.id.btnDivide);
        btnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    int i = add(Integer.parseInt(txtNumber1.getText().toString()), Integer.parseInt(txtNumber2.getText().toString()));
                    result.setText("" + i);
                } catch (Exception e) {
                    createSimpleDialog(DLG_TITLE, DLG_MESSAGE);
                }
            }
        });
        btnSubtract.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                try {
                    int i = minus(Integer.parseInt(txtNumber1.getText().toString()), Integer.parseInt(txtNumber2.getText().toString()));
                    result.setText("" + i);
                } catch (Exception e) {
                    createSimpleDialog(DLG_TITLE, DLG_MESSAGE);
                }
            }
        });
        btnMultiply.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                try {
                    int i = multiply(Integer.parseInt(txtNumber1.getText().toString()), Integer.parseInt(txtNumber2.getText().toString()));
                    result.setText("" + i);
                } catch (Exception e) {
                    createSimpleDialog(DLG_TITLE, DLG_MESSAGE);
                }
            }
        });
        btnDivide.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    int i = divide(Integer.parseInt(txtNumber1.getText().toString()), Integer.parseInt(txtNumber2.getText().toString()));
                    result.setText("" + i);
                } catch (Exception e) {
                    createSimpleDialog(DLG_TITLE, DLG_MESSAGE);
                }
            }
        });
    }

    private int add(int a, int b) {
        return a + b;
    }

    private int minus(int a, int b) {
        return a - b;
    }

    private int multiply(int a, int b) {
        return a * b;
    }

    private int divide(int a, int b) {
        return a / b;
    }

    private void createSimpleDialog(String title, String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(title);
        builder.setMessage(message);
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        builder.show();
    }
}