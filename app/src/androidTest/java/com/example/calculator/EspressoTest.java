package com.example.calculator;

import android.support.test.rule.ActivityTestRule;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.typeText;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;

public class EspressoTest {
    @Rule
    public ActivityTestRule<MainActivity> activityTestRule =
            new ActivityTestRule<>(MainActivity.class);

    // Validate Addition feature with valid input data
    @Test
    public void validateAddSuccess() {
        onView(withId(R.id.txtNumber1)).perform(typeText("1"));
        onView(withId(R.id.txtNumber2)).perform(typeText("2"));
        onView(withId(R.id.btnAdd)).perform(click());
        onView(withId(R.id.txtResult)).check(matches(withText("3")));
    }

    // Validate Subtraction feature with valid input data
    @Test
    public void validateSubtractSuccess() {
        onView(withId(R.id.txtNumber1)).perform(typeText("3"));
        onView(withId(R.id.txtNumber2)).perform(typeText("1"));
        onView(withId(R.id.btnSubtract)).perform(click());
        onView(withId(R.id.txtResult)).check(matches(withText("2")));
    }

    // Validate Multiplication feature with valid input data
    @Test
    public void validateMultiplySuccess() {
        onView(withId(R.id.txtNumber1)).perform(typeText("2"));
        onView(withId(R.id.txtNumber2)).perform(typeText("3"));
        onView(withId(R.id.btnMultiply)).perform(click());
        onView(withId(R.id.txtResult)).check(matches(withText("6")));
    }

    // Validate Division feature with valid input data
    @Test
    public void validateDivideSuccess() {
        onView(withId(R.id.txtNumber1)).perform(typeText("6"));
        onView(withId(R.id.txtNumber2)).perform(typeText("3"));
        onView(withId(R.id.btnDivide)).perform(click());
        onView(withId(R.id.txtResult)).check(matches(withText("2")));
    }

    // Validate Addition feature with invalid input data
    @Test
    public void validateAddWithOutOfInt() {
        onView(withId(R.id.txtNumber1)).perform(typeText("12345678901"));
        onView(withId(R.id.txtNumber2)).perform(typeText("12345678901"));
        onView(withId(R.id.btnAdd)).perform(click());
        onView(withText(MainActivity.DLG_MESSAGE)).check(matches(isDisplayed()));
        onView(withId(android.R.id.button1)).perform(click());
    }

    // Validate Division feature with invalid input data
    @Test
    public void validateDivideByZero() {
        onView(withId(R.id.txtNumber1)).perform(typeText("10"));
        onView(withId(R.id.txtNumber2)).perform(typeText("0"));
        onView(withId(R.id.btnDivide)).perform(click());
        onView(withText(MainActivity.DLG_MESSAGE)).check(matches(isDisplayed()));
        onView(withId(android.R.id.button1)).perform(click());
    }

}